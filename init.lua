letters = {'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'}
for i, letter in ipairs(letters) do
minetest.register_node("stonewriter:carved_stone_"..letter, {
	description = "carved letter "..letter,
	tiles = {"carved_stone"..letter..".png" },
	groups = {cracky=1}
})
end

state_obj={}
letter_form = {}
function letter_form.get_formspec(name)
	local formspec = {
		"formspec_version[4]",
		"size[3,3]",
		"field[0.375,0.375;2.25,2.25;letter;Letter;]"
	}
	return table.concat(formspec,"")
end
function letter_form.show_to(name)
	minetest.show_formspec(name, "stonewriter:letterform", letter_form.get_formspec(name))
end

minetest.register_on_player_receive_fields(function(player, formname, fields)
	if formname ~= "stonewriter:letterform" then
		return
	end
	
	if fields.letter then
		local letter = string.upper(fields.letter)
		for i, l in ipairs(letters) do
			if letter == l then
				if state_obj[player] then
					if state_obj[player].pos then
						minetest.set_node(state_obj[player].pos,{name="stonewriter:carved_stone_"..letter})
						state_obj[player] = nil
					end
				end
				break
			end
		end
	end
end)

minetest.register_tool("stonewriter:chisel",{
	description="chisel that can carve stone",
	inventory_image="chisel.png"
})

minetest.register_on_punchnode(function(pos, node, puncher, ext)
	local wielded_item = puncher:get_wielded_item()
	local item_name = wielded_item:get_name()
	if node.name == "default:stone" and item_name == "stonewriter:chisel" then
		state_obj[puncher] = {pos=pos}
		letter_form.show_to(puncher:get_player_name())
	end
end)

minetest.register_craft({
	output = "stonewriter:chisel 1",
	recipe = {
		{"","default:glass",""},
		{"","default:stick",""},
		{"","default:stick",""}
	}
})
